package com.fernandez.districts.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.fernandez.districts.dto.DistrictsDTO;
import com.fernandez.districts.model.Districts;

@Component
public class DistrictMapper {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private Environment environment;

	public List<DistrictsDTO> mapToDistrictsListDTO(List<Districts> listDistricts) {
		Type listType = new TypeToken<List<DistrictsDTO>>() {
		}.getType();
		List<DistrictsDTO> districtsDtoList = modelMapper.map(listDistricts, listType);
		return districtsDtoList;
	}

	public DistrictsDTO mapToDistrictsDTO(Districts districts) {
		Type districtsDTO = new TypeToken<DistrictsDTO>() {}.getType();
		return modelMapper.map(districts, districtsDTO);
	}
	
	public Districts mapToDistricts(DistrictsDTO districtsDTO) {
		Type districts = new TypeToken<Districts>() {}.getType();
		return modelMapper.map(districtsDTO, districts);
	}

}

