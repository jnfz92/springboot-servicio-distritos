package com.fernandez.districts.services;

import java.util.List;

import com.fernandez.districts.dto.DistrictsDTO;

public interface IDistrictsServices {

	DistrictsDTO find(Long districtsId);

	List<DistrictsDTO> findAll();

	void delete(Long districtsId);

	DistrictsDTO save(DistrictsDTO districtsDTO);

	DistrictsDTO update(DistrictsDTO districtsDTO, String idDistrict);

}
