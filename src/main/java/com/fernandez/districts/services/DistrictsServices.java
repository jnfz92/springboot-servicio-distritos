package com.fernandez.districts.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fernandez.districts.dto.DistrictsDTO;
import com.fernandez.districts.mapper.DistrictMapper;
import com.fernandez.districts.model.Districts;
import com.fernandez.districts.repository.DistrictsRepository;
import com.fernandez.districts.utils.DistrictsUtils;

@Service
public class DistrictsServices implements IDistrictsServices {

	@Autowired
	private DistrictsRepository districtsRepository;

	@Autowired
	private DistrictMapper districtsMapper;
	
	@Override
	public List<DistrictsDTO> findAll() {
		List<DistrictsDTO> districtsDTOList = new ArrayList<DistrictsDTO>();
		List<Districts> districtsList = districtsRepository.findAll();
		if (!DistrictsUtils.isNullOrEmpty(districtsList)) {
			districtsDTOList = districtsMapper.mapToDistrictsListDTO(districtsList);
		}
		return districtsDTOList;
	}

	@Override
	public DistrictsDTO save (DistrictsDTO districtsDTO) {
		return districtsMapper.mapToDistrictsDTO(districtsRepository.save(districtsMapper.mapToDistricts(districtsDTO)));
	}

	@Override
	public DistrictsDTO update (DistrictsDTO districtsDTO , String idDistrict) {
		if (districtsRepository.findById(Long.valueOf(idDistrict))!=null) {
			districtsRepository.save(districtsMapper.mapToDistricts(districtsDTO));
		}
		return null;
	}
	
	@Override
	public void delete(Long districtsId) {
		Optional<Districts> districts = findById(districtsId);
		if (districts.isPresent()) {
			districtsRepository.deleteById(districts.get().getIdDistrict());
		}

	}

	@Override
	public DistrictsDTO find (Long districtsId) {
		return districtsMapper.mapToDistrictsDTO(findById(districtsId).get());
	}
	
	private Optional<Districts> findById(Long districtsId) {
		return districtsRepository.findById(districtsId);
	}

	

}
