package com.fernandez.districts.utils;

import org.springframework.beans.factory.annotation.Value;

public class ConfigurationVariablesUtils {

	@Value("${spring.application.name}")
	public static String texto;
	
	@Value("${my.properties.some_mandatory_property}")
	public static String environment;
	
}
