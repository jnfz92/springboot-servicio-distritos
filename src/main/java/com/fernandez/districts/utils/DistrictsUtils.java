package com.fernandez.districts.utils;

import java.util.Collection;

import lombok.experimental.UtilityClass;

@UtilityClass
public class DistrictsUtils {

	public <T> boolean isNullOrEmpty(Collection<T> list) {
		return list == null || list.isEmpty();
	}

	
}
