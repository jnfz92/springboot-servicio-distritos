package com.fernandez.districts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "districts")
public class Districts {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idDistrict;
	
	@Column(name = "perimetro")
	private String perimetro;
	
	@Column(name = "codigo_distrito")
	private String codigoDistrito;
	
	@Column(name = "nombre_distrito")
	private String nombreDistrito;
	
	@Column(name = "nombre_acentuado_del_distrito")
	private String nombreAcentuadoDelDistrito;
	
	@Column(name = "numero_de_barrio")
	private String numeroDeBarrio;
	
	@Column(name = "superficie")
	private String superficie;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoDistrito == null) ? 0 : codigoDistrito.hashCode());
		result = prime * result + ((idDistrict == null) ? 0 : idDistrict.hashCode());
		result = prime * result + ((nombreAcentuadoDelDistrito == null) ? 0 : nombreAcentuadoDelDistrito.hashCode());
		result = prime * result + ((nombreDistrito == null) ? 0 : nombreDistrito.hashCode());
		result = prime * result + ((numeroDeBarrio == null) ? 0 : numeroDeBarrio.hashCode());
		result = prime * result + ((perimetro == null) ? 0 : perimetro.hashCode());
		result = prime * result + ((superficie == null) ? 0 : superficie.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Districts other = (Districts) obj;
		if (codigoDistrito == null) {
			if (other.codigoDistrito != null)
				return false;
		} else if (!codigoDistrito.equals(other.codigoDistrito))
			return false;
		if (idDistrict == null) {
			if (other.idDistrict != null)
				return false;
		} else if (!idDistrict.equals(other.idDistrict))
			return false;
		if (nombreAcentuadoDelDistrito == null) {
			if (other.nombreAcentuadoDelDistrito != null)
				return false;
		} else if (!nombreAcentuadoDelDistrito.equals(other.nombreAcentuadoDelDistrito))
			return false;
		if (nombreDistrito == null) {
			if (other.nombreDistrito != null)
				return false;
		} else if (!nombreDistrito.equals(other.nombreDistrito))
			return false;
		if (numeroDeBarrio == null) {
			if (other.numeroDeBarrio != null)
				return false;
		} else if (!numeroDeBarrio.equals(other.numeroDeBarrio))
			return false;
		if (perimetro == null) {
			if (other.perimetro != null)
				return false;
		} else if (!perimetro.equals(other.perimetro))
			return false;
		
		if (superficie == null) {
			if (other.superficie != null)
				return false;
		} else if (!superficie.equals(other.superficie))
			return false;
		return true;
	}

	public Long getIdDistrict() {
		return idDistrict;
	}

	public void setIdDistrict(Long idDistrict) {
		this.idDistrict = idDistrict;
	}

	public String getPerimetro() {
		return perimetro;
	}

	public void setPerimetro(String perimetro) {
		this.perimetro = perimetro;
	}

	public String getCodigoDistrito() {
		return codigoDistrito;
	}

	public void setCodigoDistrito(String codigoDistrito) {
		this.codigoDistrito = codigoDistrito;
	}

	public String getNombreDistrito() {
		return nombreDistrito;
	}

	public void setNombreDistrito(String nombreDistrito) {
		this.nombreDistrito = nombreDistrito;
	}

	public String getNombreAcentuadoDelDistrito() {
		return nombreAcentuadoDelDistrito;
	}

	public void setNombreAcentuadoDelDistrito(String nombreAcentuadoDelDistrito) {
		this.nombreAcentuadoDelDistrito = nombreAcentuadoDelDistrito;
	}

	public String getNumeroDeBarrio() {
		return numeroDeBarrio;
	}

	public void setNumeroDeBarrio(String numeroDeBarrio) {
		this.numeroDeBarrio = numeroDeBarrio;
	}

	public String getSuperficie() {
		return superficie;
	}

	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}

	@Override
	public String toString() {
		return "Districts [idDistrict=" + idDistrict + ", perimetro=" + perimetro + ", codigoDistrito=" + codigoDistrito
				+ ", nombreDistrito=" + nombreDistrito + ", nombreAcentuadoDelDistrito=" + nombreAcentuadoDelDistrito
				+ ", numeroDeBarrio=" + numeroDeBarrio + ", superficie=" + superficie + "]";
	}
	
	
}
