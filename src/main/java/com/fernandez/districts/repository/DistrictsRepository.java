package com.fernandez.districts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fernandez.districts.model.Districts;

public interface DistrictsRepository  extends JpaRepository<Districts, Long>{

}
