package com.fernandez.districts.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.districts.dto.DistrictsDTO;
import com.fernandez.districts.mapper.DistrictMapper;
import com.fernandez.districts.model.Districts;
import com.fernandez.districts.services.IDistrictsServices;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class DistrictsController implements IDistrictsController {

	@Autowired
	private IDistrictsServices districts;

	@Autowired
	private DistrictMapper districtsMapper;

	@GetMapping(value = "/")
	@Override
	public ResponseEntity<List<DistrictsDTO>> getDistricts() {
		return ResponseEntity.ok(districts.findAll());
	}

	@GetMapping(value = "/{idDistrict}")
	@Override
	public ResponseEntity<DistrictsDTO> getDistrictById(@PathVariable("idDistrict") String idDistrict) {
		DistrictsDTO districtsDTO = districts.find(Long.valueOf(idDistrict));
		
		if (districtsDTO!=null) {
			log.error("Id " + idDistrict + " is not existed");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(districtsDTO);
	}

	@PostMapping(value = "/")
	@Override
	public ResponseEntity<DistrictsDTO> save(@RequestBody DistrictsDTO districtsDTO) {
		return ResponseEntity.ok(districts.save(districtsDTO));
	}

	@PutMapping(value = "/{idDistrict}")
	@Override
	public ResponseEntity<DistrictsDTO> update(@PathVariable(value = "idDistrict") String idDistrict,@RequestBody DistrictsDTO districtsDTO) {
		
        Districts district = districtsMapper.mapToDistricts(districtsDTO);
        district.setIdDistrict(Long.valueOf(idDistrict));
        districts.save(districtsMapper.mapToDistrictsDTO(district));
        
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(districtsMapper.mapToDistrictsDTO(district));
        
	}

	@DeleteMapping(value = "/{idDistrict}")
	@Override
	public ResponseEntity deleteDistrictById(@PathVariable("idDistrict") Long idDistrict) {
		
		if (districts.find(idDistrict)==null) {
            log.error("Id " + idDistrict + " is not existed");
            ResponseEntity.badRequest().build();
        }

		districts.delete(idDistrict);
		
        return ResponseEntity.ok().build();
	}

}
