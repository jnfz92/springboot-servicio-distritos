package com.fernandez.districts.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.districts.utils.ConfigurationVariablesUtils;

@RestController
public class ConfigController implements IConfigController {	
	
	@GetMapping(value = "/config")
	@Override
    public ResponseEntity<?> getConfiguration() throws Exception {
		Map<String,String> json = new HashMap<>();
		json.put("texto", ConfigurationVariablesUtils.texto);
		json.put("entorno",ConfigurationVariablesUtils.environment);
        return new ResponseEntity<Map<String,String>>(json, HttpStatus.OK);
	}
	
}
