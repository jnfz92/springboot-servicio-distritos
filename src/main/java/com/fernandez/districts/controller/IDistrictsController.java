package com.fernandez.districts.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.fernandez.districts.dto.DistrictsDTO;

public interface IDistrictsController {

	ResponseEntity<List<DistrictsDTO>> getDistricts();

	ResponseEntity<DistrictsDTO> getDistrictById(String idDistrict);

	ResponseEntity<DistrictsDTO> save(DistrictsDTO districtsDTO);

	ResponseEntity<DistrictsDTO> update(String idDistrict, DistrictsDTO districtsDTO);

	ResponseEntity deleteDistrictById(Long idDistrict);

}
