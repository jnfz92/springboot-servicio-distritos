package com.fernandez.districts.controller;

import org.springframework.http.ResponseEntity;

public interface IConfigController {

	ResponseEntity<?> getConfiguration() throws Exception;

}
